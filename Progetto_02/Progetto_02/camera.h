#ifndef __CAMERA__
#define __CAMERA__

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Defines several possible options for camera movement.
enum cameraMovement {
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT,
	UP,
	DOWN
};

// Default camera values
const float YAW = -90.0f;
const float PITCH = 0.0f;
const float SPEED = 2.5f;
const float SENSITIVITY = 0.1f;

// An abstract camera class that processes input and calculates the corresponding Euler Angles, Vectors and Matrices for use in OpenGL
class Camera {
private:
	// Camera Attributes
	glm::vec3 position;
	glm::vec3 direction;
	glm::vec3 right;
	glm::vec3 up;
	glm::vec3 worldUp;
	// Euler Angles
	float yaw;
	float pitch;
	// Camera options
	float movementSpeed = 0.2f;
	float mouseSensitivityX = 1.0f;
	float mouseSensitivityY = 1.0f;
	float maxDistance;
	// Calculates the front vector from the Camera's (updated) Euler Angles
	void update();
	bool isReachable(glm::vec3 point);
public:
	// Constructor (required)
	Camera(glm::vec3 position, glm::vec3 direction, float maxDistance, glm::vec3 worldUp = glm::vec3(0.0f, 1.0f, 0.0f));
	// Constructor (required)
	Camera(glm::vec3 position, float yaw, float pitch, float maxDistance, glm::vec3 worldUp = glm::vec3(0.0f, 1.0f, 0.0f));
	// Zero Arguments Constructor (required)
	Camera();
	// Copy Constructor (required)
	Camera(const Camera &c);
	// Assignment operator (required)
	void operator=(const Camera &c);
	// Accessors
	glm::vec3 getPosition();
	glm::vec3 getDirection();
	glm::vec3 getRight();
	glm::vec3 getUp();
	glm::vec3 getWorldUp();
	// Returns the view matrix calculated using Euler Angles and the LookAt Matrix
	glm::mat4 getViewMatrix();
	// Move the camera.
	void move(cameraMovement movement);
	// Rotate the camera accordingly with the mouse movement.
	void rotate(float xoffset, float yoffset);
	// Deconstructor (required)
	~Camera();
};

#endif
