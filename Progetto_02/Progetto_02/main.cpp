﻿#include <GL\glew.h>
#include <GL\freeglut.h>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <iostream>
#include <cmath>
#include <algorithm>
#include <string>

#include "shader_maker.h"
#include "VAO_initializer.h"

#include "camera.h"
#include "texture_cube.h"
#include "texture_sand.h"
#include "test_sand.h"
#include "texture_sphere.h"
#include "skybox.h"

#include "point3d.h"
#include "color_rgba.h"
#include "index.h"
#include "light.h"
#include "material.h"

#define FRAMERATE 60 // frames per second
#define TIMESTEP (1000/FRAMERATE) // milliseconds

#define KEY_ESC 27
#define KEY_SPACE 32
#define MAX_LIGHT 128

// window settings
int width = 640;
int height = 360;

// keys state
bool pressingUp = false;
bool pressingDown = false;
bool pressingLeft = false;
bool pressingRight = false;
bool pressingTop = false;
bool pressingBot = false;

long int rotationX = 0;
long int rotationY = 0;
int alfa = 0;
int radius = 9;
int radius2 = 4;
int radius3 = 16;

//bouncing
double lato = 2;
double scaleXY = 1;
double scaleZ = 1;
double speed = -3;
double posY = 5;

// light
int numberOfLight;
Light light[MAX_LIGHT];

// material
Material material;

// texture
char const * path = "img/sand_texture.jpg";

std::string facesTexture[6] = {
	"img/rightS.jpg",
	"img/leftS.jpg",
	"img/topS.jpg",
	"img/bottomS.jpg",
	"img/frontS.jpg",
	"img/backS.jpg"
};

// geometry
TextureCube tc({ 0.0f,0.0f,0.0f }, 1.0f, { 0.5f,0.5f,0.5f,1.0f });

TestSand ts({ 0.0f,0.0f,0.0f }, 1.0f, { 0.5f,0.5f,0.5f,1.0f });

TextureSphere tsp({ 0.0f,0.0f,0.0f }, 1.0f, { 0.5f,0.5f,0.5f,1.0f });

SkyBox sb({ 0.0f,0.0f,0.0f }, 110.0f);

// camera settings
Camera camera(glm::vec3(-25.1f, -3.0f, -25.0f), glm::vec3(1.0f, 0.25f, 1.0f), sb.getEdge() / 2.0f - tc.getEdge()*2.0f);
int lastX = width / 2;
int lastY = height / 2;
bool firstMouseMovement = true;

// shaders uniform variables locations
GLuint modelSkyBoxId, projectionSkyBoxId, viewSkyBoxId;
GLuint modelShapeId, projectionShapeId, viewShapeId;
GLuint numberOfLightId;
LightId lightId[MAX_LIGHT];
MaterialId materialId;
GLuint cameraPositionId;

// VAO
GLuint SkyBoxVAO;
GLuint ShapeVAO;
GLuint programSkyBoxId;
GLuint programShapeId;

// VAOsand
GLuint sandVAO;
GLuint sphereVAO;

// matrix
glm::mat4 model;
glm::mat4 view;
glm::mat4 projection;

unsigned char *data2;
int width2, height2, nrComponents2;
unsigned char *data3;
int width3, height3, nrComponents3;
unsigned char *data4;
int width4, height4, nrComponents4;

unsigned int textureId2;
unsigned int textureId3;
unsigned int textureId4;

GLenum formatSelector(int nrComponents) {
	if (nrComponents == 1) {
		return GL_RED;
	}
	if (nrComponents == 3) {
		return GL_RGB;
	}
	if (nrComponents == 4) {
		return GL_RGBA;
	}
	return 0;
}

unsigned int loadTexture(char const * path) {

	unsigned int textureId;
	glGenTextures(1, &textureId);
	int width, height, nrComponents;
	unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);
	if (data) {
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, formatSelector(nrComponents), width, height, 0, formatSelector(nrComponents), GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	else {
		fprintf(stdout, "Failed to load texture\n");
	}
	stbi_image_free(data);

	return textureId;
}

unsigned int loadCubeMap(std::string* facesTexture) {

	unsigned int textureId;
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureId);
	int width, height, nrComponents;
	for (unsigned int i = 0; i < 6; i++) {
		unsigned char *data = stbi_load(facesTexture[i].c_str(), &width, &height, &nrComponents, 0);
		if (data) {
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, formatSelector(nrComponents), width, height, 0, formatSelector(nrComponents), GL_UNSIGNED_BYTE, data);
			stbi_image_free(data);
		}
		else {
			fprintf(stdout, "Failed to load texture\n");
			stbi_image_free(data);
		}
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	return textureId;
}

// initialization routine.
void buildScene(void) {

	// clearscreen
	GLenum ErrorCheckValue = glGetError();
	glClearColor(0.0, 0.0, 0.0, 0.0);

	// init light
	numberOfLight = 1;
	light[0].position = glm::vec4(-15.0f, 15.0f, 15.0f, 1.0f);
	light[0].ambientIntensity = glm::vec3(1.0f, 1.0f, 1.0f)*5.0f;
	light[0].diffuseIntensity = glm::vec3(0.5f, 0.5f, 0.5f)*5.0f;
	light[0].specularIntensity = glm::vec3(1.0f, 1.0f, 1.0f)*5.0f;

	// init material
	material.ambientReflectionConstant = glm::vec3(0.1f, 0.1f, 0.1f);
	material.diffuseReflectionConstant = glm::vec3(0.5f, 0.5f, 0.5f);
	material.specularReflectionConstant = glm::vec3(1.0f, 1.0f, 1.0f);
	material.shininessConstant = 1000.0f;

	// init shape shaders
	char* vertexShaderShape = (char*)"shader/TextureCubeVertexShader.glsl";
	char* fragmentShaderShape = (char*)"shader/TextureCubeFragmentShader.glsl";
	programShapeId = ShaderMaker::createProgram(vertexShaderShape, fragmentShaderShape);
	glUseProgram(programShapeId);

	// init VAO
	//sphereVAO = VAOinitializer::initialize(sphereVAO, tsp.getVertex(), tsp.getColor(), tsp.getNormal(), tsp.getTextureCoordinate(), tsp.getNumberOfVertex(), tsp.getIndex(), tsp.getNumberOfIndex());
	
	ShapeVAO = VAOinitializer::initialize(ShapeVAO, tc.getVertex(), tc.getColor(), tc.getNormal(), tc.getTextureCoordinate(), tc.getNumberOfVertex(), tc.getIndex(), tc.getNumberOfIndex());
	sandVAO = VAOinitializer::initialize(sandVAO, ts.getVertexS(), ts.getColorS(), ts.getNormalS(), ts.getTextureCoordinateS(), ts.getNumberOfVertex(), ts.getIndexS(), ts.getNumberOfIndex());
	
	
	// get uniform variable location
	modelShapeId = glGetUniformLocation(programShapeId, "model");
	viewShapeId = glGetUniformLocation(programShapeId, "view");
	projectionShapeId = glGetUniformLocation(programShapeId, "projection");

	numberOfLightId = glGetUniformLocation(programShapeId, "numberOfLight");
	for (int i = 0; i < std::min(numberOfLight, MAX_LIGHT); i++) {
		lightId[i].position = glGetUniformLocation(programShapeId, ("light[" + std::to_string(i) + "].position").c_str());
		lightId[i].ambientIntensity = glGetUniformLocation(programShapeId, ("light[" + std::to_string(i) + "].ambientIntensity").c_str());
		lightId[i].diffuseIntensity = glGetUniformLocation(programShapeId, ("light[" + std::to_string(i) + "].diffuseIntensity").c_str());
		lightId[i].specularIntensity = glGetUniformLocation(programShapeId, ("light[" + std::to_string(i) + "].specularIntensity").c_str());
	}

	materialId.ambientReflectionConstant = glGetUniformLocation(programShapeId, "material.ambientReflectionConstant");
	materialId.diffuseReflectionConstant = glGetUniformLocation(programShapeId, "material.diffuseReflectionConstant");
	materialId.specularReflectionConstant = glGetUniformLocation(programShapeId, "material.specularReflectionConstant");
	materialId.shininessConstant = glGetUniformLocation(programShapeId, "material.shininessConstant");

	cameraPositionId = glGetUniformLocation(programShapeId, "cameraPosition");

	// set uniform variable
	glUniform1i(numberOfLightId, numberOfLight);
	for (int i = 0; i < std::min(numberOfLight, MAX_LIGHT); i++) {
		glUniform4f(lightId[i].position, light[i].position.x, light[i].position.y, light[i].position.z, light[i].position.w);
		glUniform3f(lightId[i].ambientIntensity, light[i].ambientIntensity.r, light[i].ambientIntensity.g, light[i].ambientIntensity.b);
		glUniform3f(lightId[i].diffuseIntensity, light[i].diffuseIntensity.r, light[i].diffuseIntensity.g, light[i].diffuseIntensity.b);
		glUniform3f(lightId[i].specularIntensity, light[i].specularIntensity.r, light[i].specularIntensity.g, light[i].specularIntensity.b);
	}

	glUniform3f(materialId.ambientReflectionConstant, material.ambientReflectionConstant.r, material.ambientReflectionConstant.g, material.ambientReflectionConstant.b);
	glUniform3f(materialId.diffuseReflectionConstant, material.diffuseReflectionConstant.r, material.diffuseReflectionConstant.g, material.diffuseReflectionConstant.b);
	glUniform3f(materialId.specularReflectionConstant, material.specularReflectionConstant.r, material.specularReflectionConstant.g, material.specularReflectionConstant.b);
	glUniform1f(materialId.shininessConstant, material.shininessConstant);

	glUniform3f(cameraPositionId, camera.getPosition().x, camera.getPosition().y, camera.getPosition().z);

	// load cube texture
	loadTexture(path);

		
	data2 = stbi_load(path, &width2, &height2, &nrComponents2, 0);
	data3 = stbi_load("img/face2.png", &width3, &height3, &nrComponents3, 0);
	data4 = stbi_load("img/satelliti.png", &width4, &height4, &nrComponents4, 0);	
	glGenTextures(1, &textureId2);
	glGenTextures(1, &textureId3);
	glGenTextures(1, &textureId4);

	// init skybox shaders
	char* vertexShaderSkyBox = (char*)"shader/SkyBoxVertexShader.glsl";
	char* fragmentShaderSkyBox = (char*)"shader/SkyBoxFragmentShader.glsl";
	programSkyBoxId = ShaderMaker::createProgram(vertexShaderSkyBox, fragmentShaderSkyBox);
	glUseProgram(programSkyBoxId);

	// init skybox VAO
	SkyBoxVAO = VAOinitializer::initialize(SkyBoxVAO, sb.getVertex(), sb.getTextureCoordinate(), sb.getNumberOfVertex());

	// get uniform variable location
	modelSkyBoxId = glGetUniformLocation(programSkyBoxId, "model");
	viewSkyBoxId = glGetUniformLocation(programSkyBoxId, "view");
	projectionSkyBoxId = glGetUniformLocation(programSkyBoxId, "projection");

	// load skybox texture
	loadCubeMap(facesTexture);
}

// draw routine
void drawScene(void) {
	double base = 22;
	double start = -5.0;
	int altezza = -4;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindTexture(GL_TEXTURE_2D, textureId4);
	glTexImage2D(GL_TEXTURE_2D, 0, formatSelector(nrComponents4), width4, height4, 0, formatSelector(nrComponents4), GL_UNSIGNED_BYTE, data4);
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//big guard
	{
		glUseProgram(programShapeId);
		glBindVertexArray(ShapeVAO);
		model = glm::mat4(1.0);
		model = glm::translate(model, glm::vec3(sin(alfa * 3.14159f / 180.0f) * radius + (base / 2 + start - 0.5), base - 11.5, cos(alfa * 3.14159f / 180.0f) * radius + (base / 2 + start - 0.5)));
		model = glm::rotate(model, glm::radians(45.0f + rotationX), glm::vec3(1.0, 0.0, 0.0));
		model = glm::rotate(model, glm::radians(45.0f + rotationY), glm::vec3(0.0, 0.0, 1.0));
		model = glm::scale(model, glm::vec3(2.5));
		view = camera.getViewMatrix();

		// set uniform variable
		glUniformMatrix4fv(modelShapeId, 1, GL_FALSE, value_ptr(model));
		glUniformMatrix4fv(viewShapeId, 1, GL_FALSE, value_ptr(view));
		glUniformMatrix4fv(projectionShapeId, 1, GL_FALSE, value_ptr(projection));
		glUniform3f(cameraPositionId, camera.getPosition().x, camera.getPosition().y, camera.getPosition().z);

		// draw
		glDrawElements(GL_TRIANGLES, 3 * tc.getNumberOfIndex(), GL_UNSIGNED_INT, 0);
	}

	//satellite1
	{
		glUseProgram(programShapeId);
		glBindVertexArray(ShapeVAO);
		model = glm::mat4(1.0);
		model = glm::translate(model, glm::vec3(sin(5 * alfa * 3.14159f / 180.0f) * radius2 + (base / 2 + start - 0.5), sin(5 * alfa * 3.14159f / 180.0f) * radius2 / 2 + base - 5.5, cos(5 * alfa * 3.14159f / 180.0f) * radius2 + (base / 2 + start - 0.5)));
		model = glm::rotate(model, glm::radians(45.0f + rotationX * 3), glm::vec3(1.0, 0.0, 0.0));
		model = glm::rotate(model, glm::radians(45.0f + rotationY * 3), glm::vec3(0.0, 0.0, 1.0));
		model = glm::rotate(model, glm::radians(45.0f + rotationY * 3), glm::vec3(0.0, 1.0, 0.0));
		view = camera.getViewMatrix();

		// set uniform variable
		glUniformMatrix4fv(modelShapeId, 1, GL_FALSE, value_ptr(model));
		glUniformMatrix4fv(viewShapeId, 1, GL_FALSE, value_ptr(view));
		glUniformMatrix4fv(projectionShapeId, 1, GL_FALSE, value_ptr(projection));
		glUniform3f(cameraPositionId, camera.getPosition().x, camera.getPosition().y, camera.getPosition().z);

		// draw
		glDrawElements(GL_TRIANGLES, 3 * tc.getNumberOfIndex(), GL_UNSIGNED_INT, 0);
	}

	//satellite2
	{
		glUseProgram(programShapeId);
		glBindVertexArray(ShapeVAO);
		model = glm::mat4(1.0);
		model = glm::translate(model, glm::vec3(sin(5 * -alfa * 3.14159f / 180.0f) * radius2 + (base / 2 + start - 0.5), sin(5 * alfa * 3.14159f / 180.0f) * radius2 / 2 + base - 5.5, cos(5 * -alfa * 3.14159f / 180.0f) * radius2 + (base / 2 + start - 0.5)));
		model = glm::rotate(model, glm::radians(45.0f + -rotationX * 3), glm::vec3(1.0, 0.0, 0.0));
		model = glm::rotate(model, glm::radians(45.0f + -rotationY * 3), glm::vec3(0.0, 0.0, 1.0));
		model = glm::rotate(model, glm::radians(45.0f + -rotationY * 3), glm::vec3(0.0, 1.0, 0.0));
		view = camera.getViewMatrix();

		// set uniform variable
		glUniformMatrix4fv(modelShapeId, 1, GL_FALSE, value_ptr(model));
		glUniformMatrix4fv(viewShapeId, 1, GL_FALSE, value_ptr(view));
		glUniformMatrix4fv(projectionShapeId, 1, GL_FALSE, value_ptr(projection));
		glUniform3f(cameraPositionId, camera.getPosition().x, camera.getPosition().y, camera.getPosition().z);

		// draw
		glDrawElements(GL_TRIANGLES, 3 * tc.getNumberOfIndex(), GL_UNSIGNED_INT, 0);
	}

	//topppp
	{
		glUseProgram(programShapeId);
		glBindVertexArray(ShapeVAO);
		model = glm::mat4(1.0);
		model = glm::translate(model, glm::vec3((base / 2 + start - 0.5), base - 0.5, (base / 2 + start - 0.5)));
		model = glm::rotate(model, glm::radians(45.0f + rotationX * 2), glm::vec3(0.0, 1.0, 0.0));
		model = glm::scale(model, glm::vec3(2));
		view = camera.getViewMatrix();

		// set uniform variable
		glUniformMatrix4fv(modelShapeId, 1, GL_FALSE, value_ptr(model));
		glUniformMatrix4fv(viewShapeId, 1, GL_FALSE, value_ptr(view));
		glUniformMatrix4fv(projectionShapeId, 1, GL_FALSE, value_ptr(projection));
		glUniform3f(cameraPositionId, camera.getPosition().x, camera.getPosition().y, camera.getPosition().z);

		// draw
		glDrawElements(GL_TRIANGLES, 3 * tc.getNumberOfIndex(), GL_UNSIGNED_INT, 0);
	}

	//piattaforma
	{
		glUseProgram(programShapeId);
		glBindVertexArray(ShapeVAO);
		model = glm::mat4(1.0);
		model = glm::translate(model, glm::vec3(sin(-alfa * 3.14159f / 180.0f) * radius3 + (base / 2 + start - 0.5), base - 19, cos(-alfa * 3.14159f / 180.0f) * radius3 + (base / 2 + start - 0.5)));
		model = glm::rotate(model, glm::radians(0.0f -alfa), glm::vec3(0.0, 1.0, 0.0));
		model = glm::scale(model, glm::vec3(3));
		view = camera.getViewMatrix();

		// set uniform variable
		glUniformMatrix4fv(modelShapeId, 1, GL_FALSE, value_ptr(model));
		glUniformMatrix4fv(viewShapeId, 1, GL_FALSE, value_ptr(view));
		glUniformMatrix4fv(projectionShapeId, 1, GL_FALSE, value_ptr(projection));
		glUniform3f(cameraPositionId, camera.getPosition().x, camera.getPosition().y, camera.getPosition().z);

		// draw
		glDrawElements(GL_TRIANGLES, 3 * tc.getNumberOfIndex(), GL_UNSIGNED_INT, 0);
	}

	//bounce1
	{
		glUseProgram(programShapeId);
		glBindVertexArray(ShapeVAO);
		model = glm::mat4(1.0);
		model = glm::translate(model, glm::vec3(sin(-alfa * 3.14159f / 180.0f) * radius3 + (base / 2 + start - 0.5), base + posY - 17 - lato/2, cos(-alfa * 3.14159f / 180.0f) * radius3 + (base / 2 + start - 0.5)));
		model = glm::rotate(model, glm::radians(0.0f - alfa), glm::vec3(0.0, 1.0, 0.0));
		model = glm::scale(model, glm::vec3(scaleXY, scaleZ, scaleXY));
		view = camera.getViewMatrix();

		// set uniform variable
		glUniformMatrix4fv(modelShapeId, 1, GL_FALSE, value_ptr(model));
		glUniformMatrix4fv(viewShapeId, 1, GL_FALSE, value_ptr(view));
		glUniformMatrix4fv(projectionShapeId, 1, GL_FALSE, value_ptr(projection));
		glUniform3f(cameraPositionId, camera.getPosition().x, camera.getPosition().y, camera.getPosition().z);

		// draw
		glDrawElements(GL_TRIANGLES, 3 * tc.getNumberOfIndex(), GL_UNSIGNED_INT, 0);
	}

	//bounce2
	{
		glUseProgram(programShapeId);
		glBindVertexArray(ShapeVAO);
		model = glm::mat4(1.0);
		model = glm::translate(model, glm::vec3(sin(-alfa * 3.14159f / 180.0f) * radius3 + (base / 2 + start - 0.5), base - posY - 21 + lato / 2, cos(-alfa * 3.14159f / 180.0f) * radius3 + (base / 2 + start - 0.5)));
		model = glm::rotate(model, glm::radians(0.0f - alfa), glm::vec3(0.0, 1.0, 0.0));
		model = glm::scale(model, glm::vec3(scaleXY, scaleZ, scaleXY));
		view = camera.getViewMatrix();

		// set uniform variable
		glUniformMatrix4fv(modelShapeId, 1, GL_FALSE, value_ptr(model));
		glUniformMatrix4fv(viewShapeId, 1, GL_FALSE, value_ptr(view));
		glUniformMatrix4fv(projectionShapeId, 1, GL_FALSE, value_ptr(projection));
		glUniform3f(cameraPositionId, camera.getPosition().x, camera.getPosition().y, camera.getPosition().z);

		// draw
		glDrawElements(GL_TRIANGLES, 3 * tc.getNumberOfIndex(), GL_UNSIGNED_INT, 0);
	}


	///////////////////////
	/*

	{
		glUseProgram(programShapeId);
		glBindVertexArray(sphereVAO);
		model = glm::mat4(1.0);
		model = glm::translate(model, glm::vec3(2, 6, 2));
		view = camera.getViewMatrix();

		// set uniform variable
		glUniformMatrix4fv(modelShapeId, 1, GL_FALSE, value_ptr(model));
		glUniformMatrix4fv(viewShapeId, 1, GL_FALSE, value_ptr(view));
		glUniformMatrix4fv(projectionShapeId, 1, GL_FALSE, value_ptr(projection));
		glUniform3f(cameraPositionId, camera.getPosition().x, camera.getPosition().y, camera.getPosition().z);

		// draw
		glDrawElements(GL_TRIANGLES, 3 * tsp.getNumberOfIndex(), GL_UNSIGNED_INT, 0);
	}
	*/
	///////////////////////

	glBindTexture(GL_TEXTURE_2D, textureId2);
	glTexImage2D(GL_TEXTURE_2D, 0, formatSelector(nrComponents2), width2, height2, 0, formatSelector(nrComponents2), GL_UNSIGNED_BYTE, data2);
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//sand test
	
	for (int i = -30; i < 30; i++) {
		for (int j = -30; j < 30; j++) {
			glUseProgram(programShapeId);
			glBindVertexArray(sandVAO);
			model = glm::mat4(1.0);			
			model = glm::translate(model, glm::vec3(i, -4, j));
			model = glm::scale(model, glm::vec3(1, 0.15, 1));
			view = camera.getViewMatrix();

			// set uniform variable
			glUniformMatrix4fv(modelShapeId, 1, GL_FALSE, value_ptr(model));
			glUniformMatrix4fv(viewShapeId, 1, GL_FALSE, value_ptr(view));
			glUniformMatrix4fv(projectionShapeId, 1, GL_FALSE, value_ptr(projection));
			glUniform3f(cameraPositionId, camera.getPosition().x, camera.getPosition().y, camera.getPosition().z);

			// draw
			glDrawElements(GL_TRIANGLES, 3 * ts.getNumberOfIndex(), GL_UNSIGNED_INT, 0);
		}
	}

	

	glBindTexture(GL_TEXTURE_2D, textureId3);
	glTexImage2D(GL_TEXTURE_2D, 0, formatSelector(nrComponents3), width3, height3, 0, formatSelector(nrComponents3), GL_UNSIGNED_BYTE, data3);
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	while (base > 0) {

		for (int i = base - 1; i >= 0; i--) {
			{
				glUseProgram(programShapeId);
				glBindVertexArray(ShapeVAO);
				model = glm::mat4(1.0);
				model = glm::scale(model, glm::vec3(1, 1, 1));
				model = glm::translate(model, glm::vec3(start + i, altezza, start));
				view = camera.getViewMatrix();

				// set uniform variable
				glUniformMatrix4fv(modelShapeId, 1, GL_FALSE, value_ptr(model));
				glUniformMatrix4fv(viewShapeId, 1, GL_FALSE, value_ptr(view));
				glUniformMatrix4fv(projectionShapeId, 1, GL_FALSE, value_ptr(projection));
				glUniform3f(cameraPositionId, camera.getPosition().x, camera.getPosition().y, camera.getPosition().z);

				// draw
				glDrawElements(GL_TRIANGLES, 3 * tc.getNumberOfIndex(), GL_UNSIGNED_INT, 0);
			}
			{
				glUseProgram(programShapeId);
				glBindVertexArray(ShapeVAO);
				model = glm::mat4(1.0);
				model = glm::scale(model, glm::vec3(1, 1, 1));
				model = glm::translate(model, glm::vec3(start, altezza, start + i));
				view = camera.getViewMatrix();

				// set uniform variable
				glUniformMatrix4fv(modelShapeId, 1, GL_FALSE, value_ptr(model));
				glUniformMatrix4fv(viewShapeId, 1, GL_FALSE, value_ptr(view));
				glUniformMatrix4fv(projectionShapeId, 1, GL_FALSE, value_ptr(projection));
				glUniform3f(cameraPositionId, camera.getPosition().x, camera.getPosition().y, camera.getPosition().z);

				// draw
				glDrawElements(GL_TRIANGLES, 3 * tc.getNumberOfIndex(), GL_UNSIGNED_INT, 0);
			}
			{
				glUseProgram(programShapeId);
				glBindVertexArray(ShapeVAO);
				model = glm::mat4(1.0);
				model = glm::scale(model, glm::vec3(1, 1, 1));
				model = glm::translate(model, glm::vec3(start + i, altezza, start + base - 1));
				view = camera.getViewMatrix();

				// set uniform variable
				glUniformMatrix4fv(modelShapeId, 1, GL_FALSE, value_ptr(model));
				glUniformMatrix4fv(viewShapeId, 1, GL_FALSE, value_ptr(view));
				glUniformMatrix4fv(projectionShapeId, 1, GL_FALSE, value_ptr(projection));
				glUniform3f(cameraPositionId, camera.getPosition().x, camera.getPosition().y, camera.getPosition().z);

				// draw
				glDrawElements(GL_TRIANGLES, 3 * tc.getNumberOfIndex(), GL_UNSIGNED_INT, 0);
			}
			{
				glUseProgram(programShapeId);
				glBindVertexArray(ShapeVAO);
				model = glm::mat4(1.0);
				model = glm::scale(model, glm::vec3(1, 1, 1));
				model = glm::translate(model, glm::vec3(start + base - 1, altezza, start + i));
				view = camera.getViewMatrix();

				// set uniform variable
				glUniformMatrix4fv(modelShapeId, 1, GL_FALSE, value_ptr(model));
				glUniformMatrix4fv(viewShapeId, 1, GL_FALSE, value_ptr(view));
				glUniformMatrix4fv(projectionShapeId, 1, GL_FALSE, value_ptr(projection));
				glUniform3f(cameraPositionId, camera.getPosition().x, camera.getPosition().y, camera.getPosition().z);

				// draw
				glDrawElements(GL_TRIANGLES, 3 * tc.getNumberOfIndex(), GL_UNSIGNED_INT, 0);
			}
		}

		base--;
		altezza++;
		start = start + 0.5;
	}


	//skybox
	{
		glBindVertexArray(0);
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(3);

		glUseProgram(programSkyBoxId);
		glBindVertexArray(SkyBoxVAO);
		// update matrix
		model = glm::mat4(1.0);
		model = glm::scale(model, glm::vec3(sb.getEdge()));
		model = glm::translate(model, glm::vec3(sb.getCenter().x, sb.getCenter().y, sb.getCenter().z));
		view = camera.getViewMatrix();
		// set uniform variable
		glUniformMatrix4fv(modelSkyBoxId, 1, GL_FALSE, value_ptr(model));
		glUniformMatrix4fv(viewSkyBoxId, 1, GL_FALSE, value_ptr(view));
		glUniformMatrix4fv(projectionSkyBoxId, 1, GL_FALSE, value_ptr(projection));
		// draw
		glDrawArrays(GL_TRIANGLES, 0, sb.getNumberOfVertex());
	}
	glBindVertexArray(0);
	glDisableVertexAttribArray(0);

	glutSwapBuffers();
}

void updateScene(int a) {

	if (pressingUp)
		camera.move(FORWARD);
	if (pressingDown)
		camera.move(BACKWARD);
	if (pressingLeft)
		camera.move(LEFT);
	if (pressingRight)
		camera.move(RIGHT);
	if (pressingBot)
		camera.move(DOWN);
	if (pressingTop)
		camera.move(UP);

	rotationX++;
	rotationY++;

	alfa++;
	if (alfa >= 360)
		alfa = 0;

	if (posY <= lato) {
		scaleZ = posY;
		scaleXY = lato * lato / scaleZ;
	} else {
		scaleXY = scaleZ = lato;
	}

	if (posY <= lato / 4 * 3) {
		posY = lato / 4 * 3;
		speed = -speed;
	}

	speed += (-80 * a / 1000);

	posY += (speed * a / 1000);

	glutPostRedisplay();
	glutTimerFunc(TIMESTEP, updateScene, TIMESTEP);
}

void resizeWindowHandler(int w, int h) {
	glViewport(0, 0, w, h);
	projection = glm::perspective(float(glm::radians(45.0)), float(w) / float(h), 0.1f, 100.0f);
}

void keyboardPressedHandler(unsigned char key, int x, int y) {
	if (key == 'w')
		pressingUp = true;
	if (key == 's')
		pressingDown = true;
	if (key == 'a')
		pressingLeft = true;
	if (key == 'd')
		pressingRight = true;
	if (key == 'q')
		pressingBot = true;
	if (key == 'e')
		pressingTop = true;
	if (key == KEY_ESC)
		exit(0);
}

void keyboardReleasedHandler(unsigned char key, int x, int y) {
	if (key == 'w')
		pressingUp = false;
	if (key == 's')
		pressingDown = false;
	if (key == 'a')
		pressingLeft = false;
	if (key == 'd')
		pressingRight = false;
	if (key == 'q')
		pressingBot = false;
	if (key == 'e')
		pressingTop = false;
}

void mousePassiveMotionHandler(int x, int y) {
	if (firstMouseMovement)
	{
		lastX = x;
		lastY = y;
		firstMouseMovement = false;
	}
	int xoffset = x - lastX;
	int yoffset = lastY - y;
	lastX = x;
	lastY = y;
	camera.rotate((float)xoffset, (float)yoffset);
}

int main(int argc, char* argv[]) {
	GLboolean GlewInitResult;

	glutInit(&argc, argv);
	glutInitContextVersion(4, 2);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Beach 3D");
	glutFullScreen();

	GlewInitResult = glewInit();
	if (GLEW_OK != GlewInitResult) {
		fprintf(
			stderr,
			"ERROR: %s\n",
			glewGetErrorString(GlewInitResult)
		);
		exit(EXIT_FAILURE);
	}
	fprintf(stdout, "INFO: OpenGL Version: %s\n", glGetString(GL_VERSION));

	glutDisplayFunc(drawScene);
	glutReshapeFunc(resizeWindowHandler);
	glutKeyboardFunc(keyboardPressedHandler);
	glutKeyboardUpFunc(keyboardReleasedHandler);


	glutPassiveMotionFunc(mousePassiveMotionHandler);
	glutTimerFunc(TIMESTEP, updateScene, TIMESTEP);

	glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH) / 2, glutGet(GLUT_WINDOW_HEIGHT) / 2);

	glEnable(GL_DEPTH_TEST);
	glewExperimental = GL_TRUE;
	glewInit();

	/*---*/
	buildScene();
	/*---*/

	glutMainLoop();
}