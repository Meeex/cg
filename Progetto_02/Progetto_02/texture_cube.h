#ifndef __TEXTURE_CUBE__
#define __TEXTURE_CUBE__

#include <GL\freeglut.h>
#include <glm/glm.hpp>
#include <glm/glm.hpp>
#include "point3d.h"
#include "color_rgba.h"
#include "index.h"
#include "point2d.h"

class TextureCube {
private:
	Point3D center;
	float edge;
	ColorRGBA color;
public:
	// Constructor (required)
	TextureCube(Point3D center, float edge, ColorRGBA color);
	// Zero Arguments Constructor (required)
	TextureCube();
	// Copy Constructor (required)
	TextureCube(const TextureCube &s);
	// Assignment operator (required)
	void operator=(const TextureCube &s);
	// Accessors
	Point3D getCenter();
	float getEdge();
	int getNumberOfVertex();
	Point3D* getVertex();
	Point3D* getNormal();
	ColorRGBA* getColor();
	Point2D* getTextureCoordinate();
	int getNumberOfIndex();
	Index* getIndex();
	// Deconstructor (required)
	~TextureCube();
};

#endif