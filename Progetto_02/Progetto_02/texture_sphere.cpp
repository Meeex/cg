#include "texture_sphere.h"

#define FETTE 10

TextureSphere::TextureSphere(Point3D center, float edge, ColorRGBA color) {
	this->center = { center.x,center.y,center.z };
	this->edge = edge;
	this->color = { color.r, color.g, color.b, color.a };
	this->slices = this->stacks = FETTE;
}

TextureSphere::TextureSphere() {
	this->center = { 0.0f,0.0f,0.0f };
	this->edge = 1.0f;
	this->color = { 0.5f,0.5f,0.5f,1.0f };
	this->slices = this->stacks = FETTE;
}

TextureSphere::TextureSphere(const TextureSphere & s) {
	this->center = { s.center.x,s.center.y,s.center.z };
	this->edge = s.edge;
	this->color = { s.color.r, s.color.g, s.color.b, s.color.a };
	this->slices = this->stacks = FETTE;
}

void TextureSphere::operator=(const TextureSphere & s) {
	this->center = { s.center.x,s.center.y,s.center.z };
	this->edge = s.edge;
	this->color = { s.color.r, s.color.g, s.color.b, s.color.a };
}

Point3D TextureSphere::getCenter() {
	return this->center;
}

float TextureSphere::getEdge() {
	return this->edge;
}

int TextureSphere::getNumberOfVertex() {
	return (this->slices + 1) * (this->stacks + 1);
}

Point3D * TextureSphere::getVertex() {
	Point3D* vertex = new Point3D[this->getNumberOfVertex()];
	int n = 0;

	for (int i = 0; i <= stacks; ++i) {

		float V = i / (float)stacks;
		float phi = V * 3.14159f;

		// Loop Through Slices
		for (int j = 0; j <= slices; ++j) {

			float U = j / (float)slices;
			float theta = U * (3.14159f * 2);

			// Calc The Vertex Positions
			float x = center.x + edge*(cosf(theta) * sinf(phi));
			float y = center.y + edge*cosf(phi);
			float z = center.z + edge*sinf(theta) * sinf(phi);

			vertex[n] = { x, y, z };
			n++;
		}
	}

	return vertex;
}

Point3D * TextureSphere::getNormal() {
	int x = this->getNumberOfVertex();

	Point3D* normal = new Point3D[this->getNumberOfVertex()];
	Point3D* vertex = this->getVertex();

	for (int i = 0; i < x; i++) {
		Point3D n = { vertex[i].x - center.x, vertex[i].y - center.y, vertex[i].z - center.z };
		Point3D a = { n.x / (abs(n.x) + abs(n.y) + abs(n.z)), n.y / (abs(n.x) + abs(n.y) + abs(n.z)), n.z / (abs(n.x) + abs(n.y) + abs(n.z)) };

		normal[i] = a;
	}

	return normal;
}

ColorRGBA * TextureSphere::getColor() {
	ColorRGBA* color = new ColorRGBA[this->getNumberOfVertex()];
	for (int i = 0; i < this->getNumberOfVertex(); i++) {
		color[i] = this->color;
	}
	return color;
}

Point2D * TextureSphere::getTextureCoordinate() {
	Point2D* texture = new Point2D[this->getNumberOfVertex()];
	for (int i = 0; i < this->getNumberOfVertex(); i += 4)
	{
		texture[i + 0] = { 0.0f, 1.0f };
		texture[i + 1] = { 0.0f, 0.0f };
		texture[i + 2] = { 1.0f, 0.0f };
		texture[i + 3] = { 1.0f, 1.0f };
	}

	return texture;
}

int TextureSphere::getNumberOfIndex() {
	return (slices * stacks + slices) * 2;
}

Index * TextureSphere::getIndex() {
	Index* index = new Index[this->getNumberOfIndex()];
	
	for (int i = 0; i < (slices * stacks + slices) * 2; ++i) {

		index[i] = { i, i + slices + 1, i + slices };		
		i++;
		index[i] = { i + slices + 1, i, i + 1 };
	}

	return index;
}

TextureSphere::~TextureSphere() {}
