#include "test_sand.h"

#include <iostream>
#include <cmath>
#include <algorithm>
#include <string>

TestSand::TestSand(Point3D center, float edge, ColorRGBA color) {
	this->center = { center.x,center.y,center.z };
	this->edge = edge;
	this->color = { color.r, color.g, color.b, color.a };
	this->vertexS = this->getVertex();
	this->normalS = this->getNormal();
	this->colorS = this->getColor();
	this->textureCoordinateS = this->getTextureCoordinate();
	this->indexS = this->getIndex();
}

TestSand::TestSand() {
	this->center = { 0.0f,0.0f,0.0f };
	this->edge = 1.0f;
	this->color = { 0.5f,0.5f,0.5f,1.0f };
	this->vertexS = this->getVertex();
	this->normalS = this->getNormal();
	this->colorS = this->getColor();
	this->textureCoordinateS = this->getTextureCoordinate();
	this->indexS = this->getIndex();
}

TestSand::TestSand(const TestSand & s) {
	this->center = { s.center.x,s.center.y,s.center.z };
	this->edge = s.edge;
	this->color = { s.color.r, s.color.g, s.color.b, s.color.a };
	this->vertexS = this->getVertex();
	this->normalS = this->getNormal();
	this->colorS = this->getColor();
	this->textureCoordinateS = this->getTextureCoordinate();
	this->indexS = this->getIndex();
}

void TestSand::operator=(const TestSand & s) {
	this->center = { s.center.x,s.center.y,s.center.z };
	this->edge = s.edge;
	this->color = { s.color.r, s.color.g, s.color.b, s.color.a };
}

Point3D TestSand::getCenter() {
	return this->center;
}

float TestSand::getEdge() {
	return this->edge;
}

int TestSand::getNumberOfVertex() {
	return 324;
}

Point3D* TestSand::getVertexS() {
	return this->vertexS;
}

Point3D * TestSand::getVertex() {
	Point3D* vertex = new Point3D[this->getNumberOfVertex()];
	// top
	int x = 0;
	for (int i = 0; i < 360; i = i + 20) {
		for (int j = 0; j < 360; j = j + 20) {
			vertex[x] = { (float)((i - 170.0) / 338.0), (float)((sin(i * 18 * 3.14159f / (180.0f * 17)) * sin(j * 18 * 3.14159f / (180.0f * 17)))/* + sin(i * 3.14159f / 180.0f)/10 - sin(j * 3.14159f / 180.0f)/20*/), (float)((j - 170.0) / 338.0) };
			x++;
		}
	}
	return vertex;
}

Point3D * TestSand::getNormalS() {
	return this->normalS;
}

Point3D * TestSand::getNormal() {
	Point3D* normal = new Point3D[this->getNumberOfVertex()];
	Point3D* vertex = this->getVertex();

	Point3D p[4];

	for (int y = 0; y < 17; y++) {
		for (int x = 0; x < 17; x++) {
			p[0] = vertex[(y * 18) + x];
			p[1] = vertex[(y * 18) + x + 1];
			p[2] = vertex[((y + 1) * 18) + x];
			p[3] = vertex[((y + 1) * 18) + x + 1];

			Point3D v = { p[1].x - p[0].x, p[1].y - p[0].y, p[1].z - p[0].z };
			Point3D w = { p[2].x - p[0].x, p[2].y - p[0].y, p[2].z - p[0].z };
			Point3D n = { (v.x*w.z) - (v.z*w.y), (v.z*w.x) - (v.x*w.z), (v.x*w.y) - (v.y*w.x) };
			Point3D a = { n.x / (abs(n.x) + abs(n.y) + abs(n.z)), n.y / (abs(n.x) + abs(n.y) + abs(n.z)), n.z / (abs(n.x) + abs(n.y) + abs(n.z)) };

			normal[(y * 18) + x] = normal[(y * 18) + x + 1] = normal[((y + 1) * 18) + x] = a;

			v = { p[1].x - p[2].x, p[1].y - p[2].y, p[1].z - p[2].z };
			w = { p[3].x - p[2].x, p[3].y - p[2].y, p[3].z - p[2].z };
			n = { (v.x*w.z) - (v.z*w.y), (v.z*w.x) - (v.x*w.z), (v.x*w.y) - (v.y*w.x) };
			a = { n.x / (abs(n.x) + abs(n.y) + abs(n.z)), n.y / (abs(n.x) + abs(n.y) + abs(n.z)), n.z / (abs(n.x) + abs(n.y) + abs(n.z)) };

			normal[((y + 1) * 18) + x + 1] = normal[(y * 18) + x + 1] = normal[((y + 1) * 18) + x] = a;
		}
	}

	return normal;
}

ColorRGBA * TestSand::getColorS() {
	return this->colorS;
}

ColorRGBA * TestSand::getColor() {
	ColorRGBA* color = new ColorRGBA[this->getNumberOfVertex()];
	for (int i = 0; i < this->getNumberOfVertex(); i++) {
		color[i] = this->color;
	}
	return color;
}

Point2D * TestSand::getTextureCoordinateS() {
	return this->textureCoordinateS;
}

Point2D * TestSand::getTextureCoordinate() {
	Point2D* texture = new Point2D[this->getNumberOfVertex()];
	for (int i = 0; i < this->getNumberOfVertex(); i += 4)
	{
		texture[i + 0] = { 0.0f, 1.0f };
		texture[i + 1] = { 0.0f, 0.0f };
		texture[i + 2] = { 1.0f, 0.0f };
		texture[i + 3] = { 1.0f, 1.0f };
	}

	return texture;
}

int TestSand::getNumberOfIndex() {
	return 578;
}

Index * TestSand::getIndexS() {
	return this->indexS;
}

Index * TestSand::getIndex() {
	Index* index = new Index[this->getNumberOfIndex()];

	int i = 0;
	for (int y = 0; y < 17; y++) {
		for (int x = 0; x < 17; x++) {

			index[i] = { (y * 18) + x, (y * 18) + x + 1, ((y + 1) * 18) + x };
			i++;
			index[i] = { ((y + 1) * 18) + x, (y * 18) + x + 1, ((y + 1) * 18) + x + 1 };
			i++;
		}
	}

	return index;
}

TestSand::~TestSand() {}
