# ifndef __VAO_INITIALIZER__
# define __VAO_INITIALIZER__

#include <GL\glew.h>
#include "point3d.h"
#include "color_rgba.h"
#include "index.h"
#include "point2d.h"

class VAOinitializer {
public:
	static GLuint initialize(GLuint VAO, Point3D* vertex, ColorRGBA* color, Point3D* normal, Point2D* textureCoordinate, int numberOfVertex, Index* index, int numberOfIndex);
	static GLuint initialize(GLuint VAO, Point3D * vertex, Point3D * textureCoordinate, int numberOfVertex);
};

# endif
