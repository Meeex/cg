#include "camera.h"

Camera::Camera(glm::vec3 position, glm::vec3 direction, float maxDistance, glm::vec3 worldUp) {
	this->position = glm::vec3(position);
	this->direction = glm::vec3(direction);
	this->worldUp = glm::vec3(worldUp);
	this->yaw = direction.x != 0.0f || direction.z != 0.0f ? glm::degrees(atan2(direction.z, direction.x)) : YAW;
	this->pitch = direction.x != 0.0f || direction.y != 0.0f || direction.z != 0.0f ? glm::degrees(atan2(direction.y, sqrt(pow(direction.x, 2) + pow(direction.z, 2)))) : PITCH;
	this->maxDistance = maxDistance;
	update();
}

Camera::Camera(glm::vec3 position, float yaw, float pitch, float maxDistance, glm::vec3 worldUp) {
	this->position = glm::vec3(position);
	this->direction = glm::vec3(0.0f);
	this->worldUp = glm::vec3(worldUp);
	this->yaw = glm::mod(yaw, 360.0f);
	this->pitch = abs(pitch) <= 89 ? pitch : pitch > 0 ? 89.0f : -89.0f;
	this->maxDistance = maxDistance;
	update();
}

Camera::Camera() {
	this->position = glm::vec3(0.0f, 0.0f, 0.0f);
	this->direction = glm::vec3(0.0f, 0.0f, -1.0f);
	this->worldUp = glm::vec3(0.0f, 1.0f, 0.0f);
	this->yaw = yaw;
	this->pitch = pitch;
	this->maxDistance = FLT_MAX;
	update();
}

Camera::Camera(const Camera & c) {
	this->position = glm::vec3(c.position);
	this->direction = glm::vec3(c.direction);
	this->worldUp = glm::vec3(c.worldUp);
	this->yaw = yaw;
	this->pitch = pitch;
	this->maxDistance = c.maxDistance;
	update();
}

void Camera::operator=(const Camera & c) {
	this->position = glm::vec3(c.position);
	this->direction = glm::vec3(c.direction);
	this->worldUp = glm::vec3(c.worldUp);
	this->yaw = yaw;
	this->pitch = pitch;
	this->maxDistance = c.maxDistance;
	update();
}

glm::vec3 Camera::getPosition() {
	return glm::vec3(this->position);
}

glm::vec3 Camera::getDirection() {
	return glm::vec3(this->direction);
}

glm::vec3 Camera::getRight() {
	return glm::vec3(this->right);
}

glm::vec3 Camera::getUp() {
	return glm::vec3(this->up);
}

glm::vec3 Camera::getWorldUp() {
	return glm::vec3(this->worldUp);
}

glm::mat4 Camera::getViewMatrix() {
	return glm::lookAt(position, position + direction, up);
}

void Camera::move(cameraMovement movement) {
	glm::vec3 d = glm::normalize(glm::vec3(direction.x, 0.0f, direction.z)); //y: 0.0f
	glm::vec3 temp = position;
	if (movement == FORWARD) {
		temp += d * movementSpeed;
	}
	if (movement == BACKWARD) {
		temp -= d * movementSpeed;
	}
	if (movement == LEFT) {
		temp += right * movementSpeed;
	}
	if (movement == RIGHT) {
		temp -= right * movementSpeed;
	}
	if (movement == DOWN) {
		temp -= movementSpeed * glm::vec3(0.0f, 0.8f, 0.0f);
	}
	if (movement == UP) {
		temp += movementSpeed * glm::vec3(0.0f, 0.8f, 0.0f);
	}
	if (isReachable(temp)) {
		position = temp;
	}
}

void Camera::rotate(float xoffset, float yoffset) {
	xoffset *= mouseSensitivityX / 4;
	yoffset *= mouseSensitivityY / 4;
	yaw = glm::mod(yaw + xoffset, 360.0f);
	pitch += yoffset;
	if (pitch > 89.0f) {
		pitch = 89.0f;
	}
	if (pitch < -89.0f) {
		pitch = -89.0f;
	}
	update();
}

void Camera::update() {
	glm::vec3 d;
	d.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	d.y = sin(glm::radians(pitch));
	d.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	direction = glm::normalize(d);
	right = glm::normalize(glm::cross(worldUp, direction));
	up = glm::normalize(glm::cross(direction, right));
}

bool Camera::isReachable(glm::vec3 point) {
	return sqrt(pow(point.x, 2) + pow(point.z, 2)) < this->maxDistance;
}

Camera::~Camera() {}
