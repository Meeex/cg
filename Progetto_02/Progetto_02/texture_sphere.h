#ifndef __TEXTURE_SPHERE__
#define __TEXTURE_SPHERE__

#include <GL\freeglut.h>
#include <glm/glm.hpp>
#include <glm/glm.hpp>
#include "point3d.h"
#include "color_rgba.h"
#include "index.h"
#include "point2d.h"

class TextureSphere {
private:
	int stacks;
	int slices;
	Point3D center;
	float edge;
	ColorRGBA color;
public:
	// Constructor (required)
	TextureSphere(Point3D center, float edge, ColorRGBA color);
	// Zero Arguments Constructor (required)
	TextureSphere();
	// Copy Constructor (required)
	TextureSphere(const TextureSphere &s);
	// Assignment operator (required)
	void operator=(const TextureSphere &s);
	// Accessors
	Point3D getCenter();
	float getEdge();
	int getNumberOfVertex();
	Point3D* getVertex();
	Point3D* getNormal();
	ColorRGBA* getColor();
	Point2D* getTextureCoordinate();
	int getNumberOfIndex();
	Index* getIndex();
	// Deconstructor (required)
	~TextureSphere();
};

#endif
