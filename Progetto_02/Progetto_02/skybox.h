#ifndef __SKYBOX__
#define __SKYBOX__

#include <GL\freeglut.h>
#include <glm/glm.hpp>
#include <glm/glm.hpp>
#include "point3d.h"
#include "color_rgba.h"
#include "index.h"
#include "point2d.h"

class SkyBox {
private:
	Point3D center;
	float edge;
public:
	// Constructor (required)
	SkyBox(Point3D center, float edge);
	// Zero Arguments Constructor (required)
	SkyBox();
	// Copy Constructor (required)
	SkyBox(const SkyBox &s);
	// Assignment operator (required)
	void operator=(const SkyBox &s);
	// Accessors
	Point3D getCenter();
	float getEdge();
	int getNumberOfVertex();
	Point3D* getVertex();
	Point3D* getTextureCoordinate();
	// Deconstructor (required)
	~SkyBox();
};

#endif
