#include "shader_maker.h"
#include <iostream>
#include <cmath>
#include <algorithm>
#include <string>

char* ShaderMaker::readShaderSource(const char* shaderFile)
{
	FILE* fp = fopen(shaderFile, "rb");
	if (fp == NULL) {
		return NULL;
	}
	fseek(fp, 0L, SEEK_END);
	long size = ftell(fp);
	fseek(fp, 0L, SEEK_SET);
	char* buf = new char[size + 1];
	fread(buf, 1, size, fp);
	buf[size] = '\0';
	fclose(fp);
	return buf;
}

GLuint ShaderMaker::createProgram(char* vertexfilename, char *fragmentfilename)
{
	int success = 0;
	char infoLog[512];
	GLenum ErrorCheckValue = glGetError();

	glClearColor(0.0, 0.0, 0.0, 0.0);

	// read vertex shader from source file
	GLchar* VertexShader = readShaderSource(vertexfilename);
	// generate a vertex shader id
	GLuint vertexShaderId = glCreateShader(GL_VERTEX_SHADER);
	// apply an id to the vertex shader
	glShaderSource(vertexShaderId, 1, (const char**)&VertexShader, NULL);
	// compile vertex shader
	glCompileShader(vertexShaderId);

	// read fragment shader from source file
	const GLchar* FragmentShader = readShaderSource(fragmentfilename);
	// generate a fragment shader id
	GLuint fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);
	// apply an id to the fragment shader
	glShaderSource(fragmentShaderId, 1, (const char**)&FragmentShader, NULL);
	// compile fragment shader
	glCompileShader(fragmentShaderId);
	glGetShaderiv(fragmentShaderId, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(fragmentShaderId, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
	ErrorCheckValue = glGetError();
	if (ErrorCheckValue != GL_NO_ERROR)
	{
		fprintf(stderr, "ERROR: Could not create the shaders: %s \n", gluErrorString(ErrorCheckValue));
		exit(-1);
	}
	// create an executable program
	GLuint programId = glCreateProgram();
	glAttachShader(programId, vertexShaderId);
	glAttachShader(programId, fragmentShaderId);
	glLinkProgram(programId);

	return programId;
}

