#ifndef __LIGHT__
#define __LIGHT__

#include <GL\glew.h>
#include <glm/glm.hpp>

typedef struct {
	glm::vec4 position;
	glm::vec3 ambientIntensity;
	glm::vec3 diffuseIntensity;
	glm::vec3 specularIntensity;
} Light;

typedef struct {
	GLuint position;
	GLuint ambientIntensity;
	GLuint diffuseIntensity;
	GLuint specularIntensity;
} LightId;

#endif
