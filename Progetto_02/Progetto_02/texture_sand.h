#ifndef __TEXTURE_SAND__
#define __TEXTURE_SAND__

#include <GL\freeglut.h>
#include <glm/glm.hpp>
#include <glm/glm.hpp>
#include "point3d.h"
#include "color_rgba.h"
#include "index.h"
#include "point2d.h"

class TextureSand {
private:
	Point3D center;
	float edge;
	ColorRGBA color;
public:
	// Constructor (required)
	TextureSand(Point3D center, float edge, ColorRGBA color);
	// Zero Arguments Constructor (required)
	TextureSand();
	// Copy Constructor (required)
	TextureSand(const TextureSand &s);
	// Assignment operator (required)
	void operator=(const TextureSand &s);
	// Accessors
	Point3D getCenter();
	float getEdge();
	int getNumberOfVertex();
	Point3D* getVertex();
	Point3D* getNormal();
	ColorRGBA* getColor();
	Point2D* getTextureCoordinate();
	int getNumberOfIndex();
	Index* getIndex();
	// Deconstructor (required)
	~TextureSand();
};

#endif