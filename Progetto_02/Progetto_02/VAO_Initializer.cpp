#include "VAO_Initializer.h"

GLuint VAOinitializer::initialize(GLuint VAO, Point3D * vertex, ColorRGBA * color, Point3D * normal, Point2D * textureCoordinate, int numberOfVertex, Index * index, int numberOfIndex) {
	// init VAO
	GLuint vertexBuffer, colorBuffer, normalBuffer, textureCoordinateBuffer, indexBuffer;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	// vertex (layer 0)
	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, numberOfVertex * sizeof(Point3D), vertex, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// color (layer 1)
	glGenBuffers(1, &colorBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
	glBufferData(GL_ARRAY_BUFFER, numberOfVertex * sizeof(ColorRGBA), color, GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// normal (layer 2)
	glGenBuffers(1, &normalBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
	glBufferData(GL_ARRAY_BUFFER, numberOfVertex * sizeof(Point3D), normal, GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// textureCoordinate (layer 3)
	glGenBuffers(1, &textureCoordinateBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, textureCoordinateBuffer);
	glBufferData(GL_ARRAY_BUFFER, numberOfVertex * sizeof(Point2D), textureCoordinate, GL_STATIC_DRAW);
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// index
	glGenBuffers(1, &indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numberOfIndex * sizeof(Index), index, GL_STATIC_DRAW);

	delete vertex;
	delete color;
	delete normal;
	delete textureCoordinate;
	delete index;

	return VAO;
}

GLuint VAOinitializer::initialize(GLuint VAO, Point3D * vertex, Point3D * textureCoordinate, int numberOfVertex) {
	// init VAO
	GLuint vertexBuffer, textureCoordinateBuffer;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	// vertex (layer 0)
	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, numberOfVertex * sizeof(Point3D), vertex, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// textureCoordinate (layer 1)
	glGenBuffers(1, &textureCoordinateBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, textureCoordinateBuffer);
	glBufferData(GL_ARRAY_BUFFER, numberOfVertex * sizeof(Point3D), textureCoordinate, GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	delete vertex;
	delete textureCoordinate;

	return VAO;
}
