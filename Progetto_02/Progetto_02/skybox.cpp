#include "skybox.h"

SkyBox::SkyBox(Point3D center, float edge) {
	this->center = { center.x,center.y,center.z };
	this->edge = edge;
}

SkyBox::SkyBox() {
	this->center = { 0.0f,0.0f,0.0f };
	this->edge = 1.0f;
}

SkyBox::SkyBox(const SkyBox & s) {
	this->center = { s.center.x,s.center.y,s.center.z };
	this->edge = s.edge;
}

void SkyBox::operator=(const SkyBox & s) {
	this->center = { s.center.x,s.center.y,s.center.z };
	this->edge = s.edge;
}

Point3D SkyBox::getCenter() {
	return this->center;
}

float SkyBox::getEdge() {
	return this->edge;
}

int SkyBox::getNumberOfVertex() {
	return 36;
}

Point3D * SkyBox::getVertex() {
	Point3D* vertex = new Point3D[this->getNumberOfVertex()];
	//front
	vertex[0] = { -0.5f,  0.5f, -0.5f };
	vertex[1] = { -0.5f, -0.5f, -0.5f };
	vertex[2] = { 0.5f, -0.5f, -0.5f };
	vertex[3] = { 0.5f, -0.5f, -0.5f };
	vertex[4] = { 0.5f,  0.5f, -0.5f };
	vertex[5] = { -0.5f,  0.5f, -0.5f };
	//left
	vertex[6] = { -0.5f, -0.5f,  0.5f };
	vertex[7] = { -0.5f, -0.5f, -0.5f };
	vertex[8] = { -0.5f,  0.5f, -0.5f };
	vertex[9] = { -0.5f,  0.5f, -0.5f };
	vertex[10] = { -0.5f,  0.5f,  0.5f };
	vertex[11] = { -0.5f, -0.5f,  0.5f };
	//right
	vertex[12] = { 0.5f, -0.5f, -0.5f };
	vertex[13] = { 0.5f, -0.5f,  0.5f };
	vertex[14] = { 0.5f,  0.5f,  0.5f };
	vertex[15] = { 0.5f,  0.5f,  0.5f };
	vertex[16] = { 0.5f,  0.5f, -0.5f };
	vertex[17] = { 0.5f, -0.5f, -0.5f };
	//back
	vertex[18] = { -0.5f, -0.5f,  0.5f };
	vertex[19] = { -0.5f,  0.5f,  0.5f };
	vertex[20] = { 0.5f,  0.5f,  0.5f };
	vertex[21] = { 0.5f,  0.5f,  0.5f };
	vertex[22] = { 0.5f, -0.5f,  0.5f };
	vertex[23] = { -0.5f, -0.5f,  0.5f };
	//top
	vertex[24] = { -0.5f,  0.5f, -0.5f };
	vertex[25] = { 0.5f,  0.5f, -0.5f };
	vertex[26] = { 0.5f,  0.5f,  0.5f };
	vertex[27] = { 0.5f,  0.5f,  0.5f };
	vertex[28] = { -0.5f,  0.5f,  0.5f };
	vertex[29] = { -0.5f,  0.5f, -0.5f };
	//bottom
	vertex[30] = { -0.5f, -0.5f, -0.5f };
	vertex[31] = { -0.5f, -0.5f,  0.5f };
	vertex[32] = { 0.5f, -0.5f, -0.5f };
	vertex[33] = { 0.5f, -0.5f, -0.5f };
	vertex[34] = { -0.5f, -0.5f,  0.5f };
	vertex[35] = { 0.5f, -0.5f,  0.5f };
	
	return vertex;
}

Point3D * SkyBox::getTextureCoordinate() {
	Point3D* textureCoordinate = new Point3D[this->getNumberOfVertex()];
	//front
	textureCoordinate[0] = { -1.0f,  1.0f, -1.0f };
	textureCoordinate[1] = { -1.0f, -1.0f, -1.0f };
	textureCoordinate[2] = { 1.0f, -1.0f, -1.0f };
	textureCoordinate[3] = { 1.0f, -1.0f, -1.0f };
	textureCoordinate[4] = { 1.0f,  1.0f, -1.0f };
	textureCoordinate[5] = { -1.0f,  1.0f, -1.0f };
	//left
	textureCoordinate[6] = { -1.0f, -1.0f,  1.0f };
	textureCoordinate[7] = { -1.0f, -1.0f, -1.0f };
	textureCoordinate[8] = { -1.0f,  1.0f, -1.0f };
	textureCoordinate[9] = { -1.0f,  1.0f, -1.0f };
	textureCoordinate[10] = { -1.0f,  1.0f,  1.0f };
	textureCoordinate[11] = { -1.0f, -1.0f,  1.0f };
	//right
	textureCoordinate[12] = { 1.0f, -1.0f, -1.0f };
	textureCoordinate[13] = { 1.0f, -1.0f,  1.0f };
	textureCoordinate[14] = { 1.0f,  1.0f,  1.0f };
	textureCoordinate[15] = { 1.0f,  1.0f,  1.0f };
	textureCoordinate[16] = { 1.0f,  1.0f, -1.0f };
	textureCoordinate[17] = { 1.0f, -1.0f, -1.0f };
	//back
	textureCoordinate[18] = { -1.0f, -1.0f,  1.0f };
	textureCoordinate[19] = { -1.0f,  1.0f,  1.0f };
	textureCoordinate[20] = { 1.0f,  1.0f,  1.0f };
	textureCoordinate[21] = { 1.0f,  1.0f,  1.0f };
	textureCoordinate[22] = { 1.0f, -1.0f,  1.0f };
	textureCoordinate[23] = { -1.0f, -1.0f,  1.0f };
	//top
	textureCoordinate[24] = { -1.0f,  1.0f, -1.0f };
	textureCoordinate[25] = { 1.0f,  1.0f, -1.0f };
	textureCoordinate[26] = { 1.0f,  1.0f,  1.0f };
	textureCoordinate[27] = { 1.0f,  1.0f,  1.0f };
	textureCoordinate[28] = { -1.0f,  1.0f,  1.0f };
	textureCoordinate[29] = { -1.0f,  1.0f, -1.0f };
	//bottom
	textureCoordinate[30] = { -1.0f, -1.0f, -1.0f };
	textureCoordinate[31] = { -1.0f, -1.0f,  1.0f };
	textureCoordinate[32] = { 1.0f, -1.0f, -1.0f };
	textureCoordinate[33] = { 1.0f, -1.0f, -1.0f };
	textureCoordinate[34] = { -1.0f, -1.0f,  1.0f };
	textureCoordinate[35] = { 1.0f, -1.0f,  1.0f };
	
	return textureCoordinate;
}

SkyBox::~SkyBox() {}
