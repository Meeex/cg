#include "texture_cube.h"

TextureCube::TextureCube(Point3D center, float edge, ColorRGBA color) {
	this->center = { center.x,center.y,center.z };
	this->edge = edge;
	this->color = { color.r, color.g, color.b, color.a };
}

TextureCube::TextureCube() {
	this->center = { 0.0f,0.0f,0.0f };
	this->edge = 1.0f;
	this->color = { 0.5f,0.5f,0.5f,1.0f };
}

TextureCube::TextureCube(const TextureCube & s) {
	this->center = { s.center.x,s.center.y,s.center.z };
	this->edge = s.edge;
	this->color = { s.color.r, s.color.g, s.color.b, s.color.a };
}

void TextureCube::operator=(const TextureCube & s) {
	this->center = { s.center.x,s.center.y,s.center.z };
	this->edge = s.edge;
	this->color = { s.color.r, s.color.g, s.color.b, s.color.a };
}

Point3D TextureCube::getCenter() {
	return this->center;
}

float TextureCube::getEdge() {
	return this->edge;
}

int TextureCube::getNumberOfVertex() {
	return 24;
}

Point3D * TextureCube::getVertex() {
	Point3D* vertex = new Point3D[this->getNumberOfVertex()];
	// top
	vertex[0] = { -0.5f, 0.5f, -0.5f };
	vertex[1] = { -0.5f, 0.5f, 0.5f };
	vertex[2] = { 0.5f, 0.5f, 0.5f };
	vertex[3] = { 0.5f, 0.5f, -0.5f };
	//front
	vertex[4] = vertex[1];
	vertex[5] = { -0.5f, -0.5f, 0.5f };
	vertex[6] = { 0.5f, -0.5f, 0.5f };
	vertex[7] = vertex[2];
	//right
	vertex[8] = vertex[2];
	vertex[9] = vertex[6];
	vertex[10] = { 0.5f, -0.5f, -0.5f };
	vertex[11] = vertex[3];
	//back
	vertex[12] = vertex[3];
	vertex[13] = vertex[10];
	vertex[14] = { -0.5f, -0.5f, -0.5f };
	vertex[15] = vertex[0];
	//left
	vertex[16] = vertex[0];
	vertex[17] = vertex[14];
	vertex[18] = vertex[5];
	vertex[19] = vertex[1];
	//bottom
	vertex[20] = vertex[5];
	vertex[21] = vertex[14];
	vertex[22] = vertex[10];
	vertex[23] = vertex[6];
	
	return vertex;
}

Point3D * TextureCube::getNormal() {
	Point3D* normal = new Point3D[this->getNumberOfVertex()];

	// top
	for (int i = 0; i <= 3; i++) {
		normal[i] = { 0.0f, 1.0f, 0.0f };
	}
	//front
	for (int i = 4; i <= 7; i++) {
		normal[i] = { 0.0f, 0.0f, 1.0f };
	}
	//right
	for (int i = 8; i <= 11; i++) {
		normal[i] = { 1.0f, 0.0f, 0.0f };
	}
	//back
	for (int i = 12; i <= 15; i++) {
		normal[i] = { 0.0f, 0.0f, -1.0f };
	}
	//left
	for (int i = 16; i <= 19; i++) {
		normal[i] = { -1.0f, 0.0f, 0.0f };
	}
	//bottom
	for (int i = 20; i <= 23; i++) {
		normal[i] = { 0.0f, -1.0f, 0.0f };
	}

	return normal;
}

ColorRGBA * TextureCube::getColor() {
	ColorRGBA* color = new ColorRGBA[this->getNumberOfVertex()];
	for (int i = 0; i < this->getNumberOfVertex(); i++) {
		color[i] = this->color;
	}
	return color;
}

Point2D * TextureCube::getTextureCoordinate() {
	Point2D* texture = new Point2D[this->getNumberOfVertex()];
	for (int i = 0; i < this->getNumberOfVertex(); i += 4)
	{
		texture[i + 0] = { 0.0f, 1.0f };
		texture[i + 1] = { 0.0f, 0.0f };
		texture[i + 2] = { 1.0f, 0.0f };
		texture[i + 3] = { 1.0f, 1.0f };
	}

	return texture;
}

int TextureCube::getNumberOfIndex() {
	return 12;
}

Index * TextureCube::getIndex() {
	Index* index = new Index[this->getNumberOfIndex()];
	//top
	index[0] = { 0,1,2 };
	index[1] = { 0,2,3 };
	//front
	index[2] = { 4,5,7 };
	index[3] = { 5,6,7 };
	//right
	index[4] = { 8,9,10 };
	index[5] = { 8,10,11 };
	//back
	index[6] = { 12,13,15 };
	index[7] = { 13,14,15 };
	//left
	index[8] = { 16,17,18 };
	index[9] = { 16,18,19 };
	//bottom
	index[10] = { 20,21,22 };
	index[11] = { 20,22,23 };

	return index;
}

TextureCube::~TextureCube() {}
