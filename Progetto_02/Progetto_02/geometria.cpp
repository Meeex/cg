#include "stdafx.h"
#include "geometria.h"
#include "Strutture.h"

double  random_range(double min, double max) {
	return min + static_cast <double> (rand()) / (static_cast <double> (RAND_MAX / (max - min)));
}

void crea_sfera(Point centro, Point raggio, vector<Point> *vertici_Sfera, vector <GLuint> *indici_Sfera, vector <ColorRGBA> *Colori_Sfera, ColorRGBA colore)
{
	int Stacks = 30;  //numero di suddivisioni sull'asse x
	int Slices = 30;  // numero di suddivisioni sull'asse y

	Point coordinate;

	//Calc The Vertices
	for (int i = 0; i <= Stacks; ++i) {

		float V = i / (float)Stacks;
		float phi = V * glm::pi <float>();

		// Loop Through Slices
		for (int j = 0; j <= Slices; ++j) {

			float U = j / (float)Slices;
			float theta = U * (glm::pi <float>() * 2);

			// Calc The Vertex Positions
			float x = centro.x+raggio.x*(cosf(theta) * sinf(phi));
			float y = centro.y+raggio.y*cosf(phi);
			float z = centro.z+raggio.z*sinf(theta) * sinf(phi);
			
			Point val = { x,y,z };
			// Push Back Vertex Data
			vertici_Sfera->push_back(val);
		}
	}

	// Calc The Index Positions
	for (int i = 0; i < Slices * Stacks + Slices; ++i) {

		indici_Sfera->push_back(i);
		indici_Sfera->push_back(i + Slices + 1);
		indici_Sfera->push_back(i + Slices);


		indici_Sfera->push_back(i + Slices + 1);
		indici_Sfera->push_back(i);
		indici_Sfera->push_back(i + 1);
	}



	for (int i = 0; i < vertici_Sfera->size(); i++)
	{
		Colori_Sfera->push_back(colore);

	}
}

void crea_cubo(Point *vertices, ColorRGBA* vertex_colors, Indici*  index_vertices)
{
	vertices[0] = { -1.0, -1.0,  1.0 };
	vertices[1] = { 1.0, -1.0,  1.0 };
	vertices[2] = { 1.0,  1.0,  1.0 };
	vertices[3] = { -1.0,  1.0,  1.0 };
	// back
	vertices[4] = { -1.0, -1.0, -1.0 };
	vertices[5] = { 1.0, -1.0, -1.0 };
	vertices[6] = { 1.0,  1.0, -1.0 };
	vertices[7] = { -1.0,  1.0, -1.0 };


	vertex_colors[0] = { 1.0, 0.0, 0.0, 1.0 };
	vertex_colors[1] = { 0.0, 1.0, 0.0,1.0 };
	vertex_colors[2] = { 0.0, 0.0, 1.0,1.0 };
	vertex_colors[3] = { 1.0, 1.0, 1.0,1.0 };
	vertex_colors[4] = { 1.0, 0.0, 0.0,1.0 };
	vertex_colors[5] = { 0.0, 1.0, 0.0,1.0 };
	vertex_colors[6] = { 0.0, 0.0, 1.0,1.0 };
	vertex_colors[7] = { 1.0, 1.0, 1.0,1.0 };

	index_vertices[0] = { 0, 1, 2 };
	index_vertices[1] = { 2, 3, 0 };
	index_vertices[2] = { 1, 5, 6 };
	index_vertices[3] = { 6, 2, 1 };
	index_vertices[4] = { 7, 6, 5 };
	index_vertices[5] = { 5, 4, 7 };
	index_vertices[6] = { 4, 0, 3 };
	index_vertices[7] = { 3, 7, 4 };
	index_vertices[8] = { 4, 5, 1 };
	index_vertices[9] = { 1, 0, 4 };
	index_vertices[10] = { 3, 2, 6 };
	index_vertices[11] = { 6, 7, 3 };

}

void crea_piramide(Point *vertices, ColorRGBA *vertex_colors, Indici  *index_vertices)
{
	vertices[0] = { -1.0, 0.0,  1.0 };
	vertices[1] = { 1.0, 0.0,  1.0 };
	vertices[2] = { 1.0, 0.0, -1.0 };
	vertices[3] = { -1.0,  0.0,  -1.0 };
	// vertice piramide
	vertices[4] = { 0.0,1.0,0.0 };


	vertex_colors[0] = { 1.0, 0.5, 0.0, 1.0 };
	vertex_colors[1] = { 0.5, 1.0, 0.0,1.0 };
	vertex_colors[2] = { 0.0, 0.5, 1.0,1.0 };
	vertex_colors[3] = { 1.0, 1.0, 1.0,1.0 };
	vertex_colors[4] = { 1.0, 0.5, 0.0,1.0 };

	index_vertices[0] = { 0, 1, 2 };
	index_vertices[1] = { 0,2,3 };
	index_vertices[2] = { 0,4,3 };
	index_vertices[3] = { 0, 1, 4 };
	index_vertices[4] = { 3,2,4 };
	index_vertices[5] = { 1,2,4 };

}

void crea_piano(Point *vertices, ColorRGBA *vertex_colors, Indici  *index_vertices)
{
	vertices[0] = { -1.0, 0.0,  1.0 };
	vertices[1] = { 1.0, 0.0,  1.0 };
	vertices[2] = { 1.0, 0.0, -1.0 };
	vertices[3] = { -1.0,  0.0,  -1.0 };
	


	vertex_colors[0] = { 1.0, 1.0, 1.0, 1.0 };
	vertex_colors[1] = { 1.0, 1.0, 1.0, 1.0 };
	vertex_colors[2] = { 0.0, 0.0, 0.0,1.0 };
	vertex_colors[3] = { 0.0, 0.0, 0.0,1.0 };

	index_vertices[0] = { 0, 1, 2 };
	index_vertices[1] = { 0,2,3 };

}

void crea_toro(vector<Point> *vertici_Toro, vector <GLuint> *indici_Toro, vector <ColorRGBA> *Colori_Toro, ColorRGBA colore)
{
	int Stacks = 30;  //numero di suddivisioni sull'asse x
	int Slices = 30;  // numero di suddivisioni sull'asse y
	float R = 1, r = 0.5;
	Point coordinate;

	//Calc The Vertices
	for (int i = 0; i <= Stacks; ++i) {

		float V = i / (float)Stacks;
		float phi = V * glm::pi <float>() * 2;

		// Loop Through Slices
		for (int j = 0; j <= Slices; ++j) {

			float U = j / (float)Slices;
			float theta = U * (glm::pi <float>() * 2);

			// Calc The Vertex Positions
			float x = (R + r*cosf(phi)) * cosf(theta);
			float y = r*sinf(phi);
			float z = (R + r*cosf(phi))* sinf(theta);


			// Push Back Vertex Data
			vertici_Toro->push_back({ x,y,z });
		}
	}

	// Calc The Index Positions
	for (int i = 0; i < Slices * Stacks + Slices; ++i) {

		indici_Toro->push_back(i);
		indici_Toro->push_back(i + Slices + 1);
		indici_Toro->push_back(i + Slices);


		indici_Toro->push_back(i + Slices + 1);
		indici_Toro->push_back(i);
		indici_Toro->push_back(i + 1);
	}


	for (int i = 0; i < vertici_Toro->size(); i++)
	{
		colore.r = random_range(0.0, 0.5);
		colore.g = random_range(0.0, 0.3);
		colore.b = random_range(0.0, 0.1);
		colore.a = 1;
		Colori_Toro->push_back(colore);

	}
}

void crea_cono(vector<Point> *vertici_Cono, vector <GLuint>* indici_Cono, vector <ColorRGBA> *Colori_Cono, ColorRGBA colore)
{
	int Stacks = 30;  //numero di suddivisioni sull'asse x
	int Slices = 30;  // numero di suddivisioni sull'asse y

	Point coordinate;

	//Calc The Vertices
	for (int i = 0; i <= Stacks; ++i) {

		float V = i / (float)Stacks;
		float h = V ;

		// Loop Through Slices
		for (int j = 0; j <= Slices; ++j) {

			float U = j / (float)Slices;
			float theta = U * (glm::pi <float>() * 2);

			// Calc The Vertex Positions
			float x =h*cosf(theta);
			float y = h;
			float z = h*sinf(theta);


			// Push Back Vertex Data
			vertici_Cono->push_back({ x,y,z });
		}
	}

	// Calc The Index Positions
	for (int i = 0; i < Slices * Stacks + Slices; ++i) {

		indici_Cono->push_back(i);
		indici_Cono->push_back(i + Slices + 1);
		indici_Cono->push_back(i + Slices);


		indici_Cono->push_back(i + Slices + 1);
		indici_Cono->push_back(i);
		indici_Cono->push_back(i + 1);
	}


	for (int i = 0; i < vertici_Cono->size(); i++)
	{
		Colori_Cono->push_back(colore);

	}
}
void crea_cilindro(vector<Point> *vertici_Cilindro, vector <GLuint> *indici_Cilindro, vector <ColorRGBA> *Colori_Cilindro, ColorRGBA colore)
{
	int Stacks = 30;  //numero di suddivisioni sull'asse x
	int Slices = 30;  // numero di suddivisioni sull'asse y

	Point coordinate;

	//Calc The Vertices
	for (int i = 0; i <= Stacks; ++i) {

		float V = i / (float)Stacks;
		float h = V ;

		// Loop Through Slices
		for (int j = 0; j <= Slices; ++j) {

			float U = j / (float)Slices;
			float theta = U * (glm::pi <float>() * 2);

			// Calc The Vertex Positions
			float x = cosf(theta);
			float y = h;
			float z = sinf(theta);


			// Push Back Vertex Data
			vertici_Cilindro->push_back({ x,y,z });
		}
	}

	// Calc The Index Positions
	for (int i = 0; i < Slices * Stacks + Slices; ++i) {

		indici_Cilindro->push_back(i);
		indici_Cilindro->push_back(i + Slices + 1);
		indici_Cilindro->push_back(i + Slices);


		indici_Cilindro->push_back(i + Slices + 1);
		indici_Cilindro->push_back(i);
		indici_Cilindro->push_back(i + 1);
	}

	
	for (int i = 0; i < vertici_Cilindro->size(); i++)
	{
		Colori_Cilindro->push_back(colore);

	}

}

