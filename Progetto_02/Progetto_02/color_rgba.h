#ifndef __COLOR_RGBA__
#define __COLOR_RGBA__

#define RGBA(x) (x / 255.0f)

typedef struct {
	float r, g, b, a;
} ColorRGBA;

#endif
