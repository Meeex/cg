#version 420 core

in vec3 fragmentColor;
in vec2 textureCoordinateOut;/* for texture */

uniform sampler2D textureSampler;/* for texture */

out vec3 color;

void main(){
	
	color = texture(textureSampler,textureCoordinateOut).rgb*fragmentColor;
	
}