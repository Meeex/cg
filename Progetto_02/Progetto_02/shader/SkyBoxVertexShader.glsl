#version 420 core

layout(location = 0) in vec3 vertex;
layout(location = 0) in vec3 textureCoordinate;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 textureCoordinateOut;

void main() {
	// skybox must be centered around the camera position
	gl_Position = projection*(mat4(mat3(view)))*model*vec4(vertex, 1);

	textureCoordinateOut = textureCoordinate;
}
