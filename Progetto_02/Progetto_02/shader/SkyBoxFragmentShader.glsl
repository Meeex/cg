#version 420 core

in vec3 fragmentColor;
in vec3 textureCoordinateOut;

uniform samplerCube textureSampler;

out vec3 color;

void main() {

	color = texture(textureSampler, textureCoordinateOut).rgb;

}
