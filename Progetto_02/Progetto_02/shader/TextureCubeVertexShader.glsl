#version 420 core

#define MAX_LIGHT 128

struct Light {
	vec4 position;
	vec3 ambientIntensity;
	vec3 diffuseIntensity;
	vec3 specularIntensity;
};

struct Material {
	vec3 ambientReflectionConstant;
	vec3 diffuseReflectionConstant;
	vec3 specularReflectionConstant;
	float shininessConstant;
};

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 color;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec2 textureCoordinate;/* for texture */

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform int numberOfLight;
uniform Light light[MAX_LIGHT];
uniform Material material;
uniform vec3 cameraPosition;

out vec3 fragmentColor;
out vec2 textureCoordinateOut;/* for texture */

void main() {
	
	vec3 vertexWCS = (model* vec4(vertex, 1)).xyz;
	vec3 viewDirection = normalize(cameraPosition - vertexWCS);
	vec3 vertexNormal = normalize(model*vec4(normal, 0.0)).xyz;

	fragmentColor = vec3(0.0);

	for (int i = 0; i < min(numberOfLight, MAX_LIGHT); ++i) {
		vec3 lightDirection = normalize(light[i].position.xyz - vertexWCS);
		vec3 reflectionDirection = -normalize(reflect(lightDirection, vertexNormal));

		vec3 ambient = material.ambientReflectionConstant * light[i].ambientIntensity;
		vec3 diffuse = max(dot(lightDirection, vertexNormal), 0.0) * material.diffuseReflectionConstant * light[i].diffuseIntensity;
		vec3 specular = pow(max(dot(reflectionDirection, viewDirection), 0.0), material.shininessConstant) * material.specularReflectionConstant * light[i].specularIntensity;

		fragmentColor = (ambient + diffuse + specular);
	}

	gl_Position = projection*view*model*vec4(vertex, 1);
	
	textureCoordinateOut = textureCoordinate;/* for texture */
}
