#ifndef __TEST_SAND__
#define __TEST_SAND__

#include <GL\freeglut.h>
#include <glm/glm.hpp>
#include <glm/glm.hpp>
#include "point3d.h"
#include "color_rgba.h"
#include "index.h"
#include "point2d.h"

class TestSand {
private:
	Point3D center;
	float edge;
	ColorRGBA color;
	Point3D* vertexS;
	Point3D* normalS;
	ColorRGBA* colorS;
	Point2D* textureCoordinateS;
	Index* indexS;
public:
	// Constructor (required)
	TestSand(Point3D center, float edge, ColorRGBA color);
	// Zero Arguments Constructor (required)
	TestSand();
	// Copy Constructor (required)
	TestSand(const TestSand &s);
	// Assignment operator (required)
	void operator=(const TestSand &s);
	// Accessors
	Point3D getCenter();
	float getEdge();
	int getNumberOfVertex();
	Point3D* getVertex();
	Point3D* getVertexS();
	Point3D* getNormal();
	Point3D* getNormalS();
	ColorRGBA* getColor();
	ColorRGBA* getColorS();
	Point2D* getTextureCoordinate();
	Point2D* getTextureCoordinateS();
	int getNumberOfIndex();
	Index* getIndex();
	Index* getIndexS();
	// Deconstructor (required)
	~TestSand();
};

#endif
