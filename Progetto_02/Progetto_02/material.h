#ifndef __MATERIAL__
#define __MATERIAL__

#include <GL\glew.h>
#include <glm/glm.hpp>

typedef struct {
	glm::vec3 ambientReflectionConstant;
	glm::vec3 diffuseReflectionConstant;
	glm::vec3 specularReflectionConstant;
	float shininessConstant;
} Material;

typedef struct {
	GLuint ambientReflectionConstant;
	GLuint diffuseReflectionConstant;
	GLuint specularReflectionConstant;
	GLuint shininessConstant;
} MaterialId;

#endif
