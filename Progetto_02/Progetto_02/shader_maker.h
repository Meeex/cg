# ifndef __SHADER_MAKER__
# define __SHADER_MAKER__

#define _CRT_SECURE_NO_WARNINGS
#include <GL\glew.h>
#include <iostream>

class ShaderMaker {
public:
	static GLuint createProgram(char* vertexfilename, char *fragmentfilename);
	static char* readShaderSource(const char* shaderFile);

private:
	ShaderMaker() { }
};

# endif
