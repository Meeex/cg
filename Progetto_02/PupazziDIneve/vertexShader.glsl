#version 420 core

layout(location=0) in vec3 vertexPosition_modelSpace;

layout(location=1) in vec3 vertexColor;

uniform mat4 MV;

out vec3 Color;

void main(){

	gl_Position=MV*vec4(vertexPosition_modelSpace,1);

	Color=vertexColor;
}