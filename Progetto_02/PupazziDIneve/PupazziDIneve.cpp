﻿// PupazziDIneve.cpp : definisce il punto di ingresso dell'applicazione console.
//

#include "stdafx.h"
#include "Lib.h"
#include "ShaderMaker.h"
#include "geometria.h"
#include "GeneraBuffer.h"



mat4 View;
mat4 MV;
mat4 Projection;


//Strutture per il piano

Point *verticesPiano;
ColorRGBA *vertex_colorsPiano;
Indici *index_verticesPiano;



//Strutture per la sfera
vector <Point> vertici_Corpo, vertici_Testa, vertici_OcchioS, vertici_OcchioD, vertici_Naso,  vertici_Snowman, vertici_albero, vertici_cono,vertici_cilindro;
vector <ColorRGBA> colori_Corpo, colori_Testa, colori_OcchioS, colori_OcchioD, colori_Naso, colori_Snowman,colori_Mondo, colori_albero, colori_cono, colori_cilindro;
vector <GLuint> indici_Corpo, indici_Testa, indici_OcchioS, indici_OcchioD, indici_Naso, indici_Snowman, indici_Mondo, indici_albero, indici_cono, indici_cilindro;

vec3 camera_pos(0, 2, 20), direzione(0.0, 0.0, -1.0), camera_up(0.0, 1.0, 0.0);

//identificativo del program shader
GLuint programId;
//Definizione dei VAO

GLuint VAO_SNOWMAN, VAO_PIANO, VAO_ALBERO;

//Locazione della matrice ModelViewProjection nello shader
GLuint MatrixID;

//Parametro telecamera
float angolo_rotY = -90.0f, angolo_rotX = 0.0f, lastX = 400.0, lastY = 400.0;;

float deltaTime = 0.0f;
float lastFrame = 0.0f;


void costruisci_albero() {
	ColorRGBA colore = { 0.0,1.0,0.0,1.0 };

//La funzione cono crea un cono con il vertice in (0,0,0) (a punta in giù) di raggio 1

	crea_cono(&vertici_cono, &indici_cono, &colori_cono, colore);

//E' necessario ruotarlo di 180 intorno all'asse x, alzarlo in posizione (0.0,5.0,0.0)
// e scalarlo solo nella direzione y per creare la chioma dell'albero
	mat4 model = mat4(1.0);
	vec4 v_temp = vec4(0.0);
	model = translate(model, vec3{ 0.0,5.0,0.0 });
	model = rotate(model, radians(180.0f), vec3(1.0, 0.0, 0.0));
	model = scale(model, vec3(1, 4, 1));
	for (int i = 0; i < vertici_cono.size(); i++)
	{
		vec4 vv = vec4(vertici_cono[i].x, vertici_cono[i].y, vertici_cono[i].z, 1.0);
		v_temp = model*vv;
		vertici_cono[i].x = v_temp.x;
		vertici_cono[i].y = v_temp.y;
		vertici_cono[i].z = v_temp.z;
	}

	//Il trono dell'albero e' un cilindro
	colore = { 165.0f / 255.0f,42.0f / 255.0f,42.0f / 255.0f,1.0 };

	//La funzione cilindro forma un cilindo con centro l'origine e raggio 1
	crea_cilindro(&vertici_cilindro, &indici_cilindro, &colori_cilindro, colore);
	model = mat4(1.0);
	v_temp = vec4(0.0);
	model = scale(model, vec3(0.25, 4.0, 0.25));
	for (int i = 0; i < vertici_cilindro.size(); i++)
	{
		vec4 vv = vec4(vertici_cilindro[i].x, vertici_cilindro[i].y, vertici_cilindro[i].z, 1.0);
		v_temp = model*vv;
		vertici_cilindro[i].x = v_temp.x;
		vertici_cilindro[i].y = v_temp.y;
		vertici_cilindro[i].z = v_temp.z;
	}


	for (int ii = 0; ii < indici_cilindro.size(); ii++)
	{
		indici_cilindro[ii] = indici_cilindro[ii] + vertici_cono.size();

	}

	vertici_albero.insert(vertici_albero.end(), vertici_cono.begin(), vertici_cono.end());
	vertici_albero.insert(vertici_albero.end(), vertici_cilindro.begin(), vertici_cilindro.end());

	indici_albero.insert(indici_albero.end(), indici_cono.begin(), indici_cono.end());
	indici_albero.insert(indici_albero.end(), indici_cilindro.begin(), indici_cilindro.end());


	colori_albero.insert(colori_albero.end(), colori_cono.begin(), colori_cono.end());
	colori_albero.insert(colori_albero.end(), colori_cilindro.begin(), colori_cilindro.end());
}

void costruisci_pupazzo_di_neve(void)
{
	ColorRGBA colore =colore = { 1.0f,1.0f,1.0f,1.0f };
	Point centro = { 0.0f, 0.75f, 0.0f };
	Point raggio = { 0.75f, 0.75f, 0.75f };

	//Crea la sfera del corpo
	crea_sfera(centro, raggio, &vertici_Corpo, &indici_Corpo, &colori_Corpo, colore);

	colore = { 1.0f,1.0f,1.0f,1.0f };
	centro = { 0.0f, 1.75f, 0.0f };
	raggio = { 0.25f, 0.25f, 0.25f };

	//crea la sfera della testa
	crea_sfera(centro, raggio, &vertici_Testa, &indici_Testa, &colori_Testa, colore);

	for (int ii = 0; ii < indici_Testa.size(); ii++)
	{
		indici_Testa[ii] = indici_Testa[ii] + vertici_Corpo.size();

	}

	//Crea l'occhio sinistro
	colore = { 0.0f,0.0f,0.0f,1.0f };
	centro = { 0.1f, 1.75f, 0.3f };
	raggio = { 0.05f, 0.05f, 0.05f };
	
	crea_sfera(centro, raggio, &vertici_OcchioS, &indici_OcchioS, &colori_OcchioS, colore);

	for (int ii = 0; ii < indici_OcchioS.size(); ii++)
	{
		indici_OcchioS[ii] = indici_OcchioS[ii] + vertici_Corpo.size() + vertici_Testa.size();

	}


	//Crea l'occhio destro

	centro = { -0.1f, 1.75f, 0.3f };
	raggio = { 0.05f, 0.05f, 0.05f };
	crea_sfera(centro, raggio, &vertici_OcchioD, &indici_OcchioD, &colori_OcchioD, colore);

	for (int ii = 0; ii < indici_OcchioD.size(); ii++)
	{
		indici_OcchioD[ii] = indici_OcchioD[ii] + vertici_Corpo.size() + vertici_Testa.size() + vertici_OcchioS.size();

	}

	//Crea il naso

		colore = { 1.0f,0.5f,0.5f,1.0f };
		crea_cono(&vertici_Naso, &indici_Naso, &colori_Naso, colore);
		mat4 model(1.0);
		vec4 v_temp;
		model = translate(model, vec3(0.0f, 1.70f,0.3f));
		model = rotate(model, radians(90.0f), vec3(1.0, 0.0, 0.0));
		model = scale(model, vec3(0.05, 0.05, 0.05));
		for (int i = 0; i < vertici_Naso.size(); i++)
		{
			vec4 vv = vec4(vertici_Naso[i].x, vertici_Naso[i].y, vertici_Naso[i].z, 1.0);
			v_temp = model*vv;
			vertici_Naso[i].x = v_temp.x;
			vertici_Naso[i].y = v_temp.y;
			vertici_Naso[i].z = v_temp.z;
		}

		for (int ii = 0; ii < indici_Naso.size(); ii++)
		{
			indici_Naso[ii] = indici_Naso[ii] + vertici_Corpo.size() + vertici_Testa.size() + vertici_OcchioS.size() + vertici_OcchioD.size();

		}

		//Crea il vector vertici_Snowman appendendo tutte le parti che lo compongono

	vertici_Snowman.reserve(vertici_Corpo.size() + vertici_Testa.size() + vertici_OcchioS.size() + vertici_OcchioD.size() + vertici_Naso.size());
	vertici_Snowman.insert(vertici_Snowman.end(), vertici_Corpo.begin(), vertici_Corpo.end());
	vertici_Snowman.insert(vertici_Snowman.end(), vertici_Testa.begin(), vertici_Testa.end());
	vertici_Snowman.insert(vertici_Snowman.end(), vertici_OcchioS.begin(), vertici_OcchioS.end());
	vertici_Snowman.insert(vertici_Snowman.end(), vertici_OcchioD.begin(), vertici_OcchioD.end());
	vertici_Snowman.insert(vertici_Snowman.end(), vertici_Naso.begin(), vertici_Naso.end());


	indici_Snowman.insert(indici_Snowman.end(), indici_Corpo.begin(), indici_Corpo.end());
	indici_Snowman.insert(indici_Snowman.end(), indici_Testa.begin(), indici_Testa.end());
	indici_Snowman.insert(indici_Snowman.end(), indici_OcchioS.begin(), indici_OcchioS.end());
	indici_Snowman.insert(indici_Snowman.end(), indici_OcchioD.begin(), indici_OcchioD.end());
	indici_Snowman.insert(indici_Snowman.end(), indici_Naso.begin(), indici_Naso.end());



	colori_Snowman.insert(colori_Snowman.end(), colori_Corpo.begin(), colori_Corpo.end());
	colori_Snowman.insert(colori_Snowman.end(), colori_Testa.begin(), colori_Testa.end());
	colori_Snowman.insert(colori_Snowman.end(), colori_OcchioS.begin(), colori_OcchioS.end());
	colori_Snowman.insert(colori_Snowman.end(), colori_OcchioD.begin(), colori_OcchioD.end());
	colori_Snowman.insert(colori_Snowman.end(), colori_Naso.begin(), colori_Naso.end());

	//Rilascia i vector

	vertici_Corpo.clear();
	colori_Corpo.clear();
	indici_Corpo.clear();

	vertici_Testa.clear();
	colori_Testa.clear();
	indici_Testa.clear();

	vertici_OcchioD.clear();
	colori_OcchioD.clear();
	indici_OcchioD.clear();

	vertici_OcchioS.clear();
	colori_OcchioS.clear();
	indici_OcchioS.clear();

	vertici_Naso.clear();
	colori_Naso.clear();
	indici_Naso.clear();
}

void costruisci_scena(void)
{
	Point centro, raggio;

	costruisci_albero();
	costruisci_pupazzo_di_neve();
	
	verticesPiano = new Point[4];
	vertex_colorsPiano = new ColorRGBA[4];
	index_verticesPiano = new Indici[2];
	crea_piano(verticesPiano, vertex_colorsPiano, index_verticesPiano);

};


// Initialization routine.
void INIT_VAO(void)
{
	GLenum ErrorCheckValue = glGetError();
	glClearColor(0.0, 0.0, 0.0, 0.0);
	char* vertexShader = (char*)"vertexShader.glsl";
	char* fragmentShader = (char*)"fragmentShader.glsl";

	//Creo il programma in cui sono linkati i due shader compilati
	programId = ShaderMaker::createProgram(vertexShader, fragmentShader);

	//Lo utilizzo

	glUseProgram(programId);

	//Creao il VAO del PIPAZZO DI NEVE

	VAO_SNOWMAN = crea_VAO_Vector(VAO_SNOWMAN, vertici_Snowman, indici_Snowman, colori_Snowman);
	
	//Crea il VAO dell'albero
	VAO_ALBERO = crea_VAO_Vector(VAO_ALBERO, vertici_albero, indici_albero, colori_albero);


	//Crea il VAO del Piano
	VAO_PIANO = crea_VAO_Puntatori(VAO_PIANO, verticesPiano, 4, vertex_colorsPiano, index_verticesPiano, 2);

}
void drawScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	float currentFrame = glutGet(GLUT_ELAPSED_TIME);
	deltaTime = (currentFrame - lastFrame) / 1000;
	lastFrame = currentFrame;


	
	//Genero la matrice della Telecamera, (matrice di Vista)
	mat4 MV,model;

	View = lookAt(camera_pos, camera_pos + direzione, camera_up);
	//Richiedo a program la locazione della variabile MV

	
	glutSwapBuffers();


}
// OpenGL window reshape routine.
void resize(int w, int h)
{
	glViewport(0, 0, w, h);

	//Definisco la matrice di Proiezione
	Projection = perspective(float(radians(45.0)), float(w) / float(h), 0.2f, 100.0f);

}

void MousePassiveMotion(int x, int y)
{
	float xoffset, yoffset;

	//Memorizzo in xoffset la distanza tra l' ascissa della posizione attuale del mouse e quella precedente
	xoffset = x - lastX;
	//Memorizzo in yoffset la distanza tra l' ordinata della posizione attuale del mouse e quella precedente
	yoffset = y - lastY;

	float parametro_sensibilita_mouse = 0.005f;

	//Riduco la distanza in pixel in percentuale

	xoffset *= parametro_sensibilita_mouse;
	yoffset *= parametro_sensibilita_mouse;

	//Memorizzo  la posizione attuale del mouse
	lastX = x;
	lastY = y;

	//Incremento l'angolo di Yaw (ruotazione intorno all'asse y) di una quantit� pari all'incremento subito dalla posizione x del mouse 
	angolo_rotY += xoffset;
	//Incremento l'angolo di Pitch (ruotazione intorno all'asse x) di una quantit� pari all'incremento subito dalla posizione y del mouse 
	angolo_rotX += yoffset;

	//Ricavo la direzone corrispondente all'angolo di Yaw e Pitch aggiornati
	//(facendo uso delle coordinate sferiche)

	vec3 dir_temp;
	dir_temp.x = cos(radians(angolo_rotY))*cos(radians(angolo_rotX));
	dir_temp.y = sin(radians(angolo_rotX));
	dir_temp.z = sin(radians(angolo_rotY))*cos(radians(angolo_rotX));

	//Normalizzo la direzione
	direzione = normalize(dir_temp);
	glutPostRedisplay();

}
// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
	float cameraSpeed = deltaTime;

	//Sposto in avanti la camera nella direzione 
	if (key == 'w')
		camera_pos += cameraSpeed*direzione;

	//Sposto indietro la camera nella direzione 
	if (key == 's')
		camera_pos -= cameraSpeed*direzione;

	//Sposto a sinsitra la camera (nella direzione ortogonale alla direzione di vista e l'alto della telecamera
	if (key == 'a')
		camera_pos -= cameraSpeed*normalize(cross(direzione, camera_up));

	//Sposto a destra la camera (nella direzione ortogonale alla direzione di vista e l'alto della telecamera
	if (key == 'd')
		camera_pos += cameraSpeed*normalize(cross(direzione, camera_up));

	glutPostRedisplay();
}





// Main routine.
int main(int argc, char* argv[])
{
	glutInit(&argc, argv);

	glutInitContextVersion(4, 2);
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);

	glutInitWindowSize(800, 800);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Pupazzi di Neve");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);
	glutPassiveMotionFunc(MousePassiveMotion);

	glEnable(GL_DEPTH_TEST);
	//	glutTimerFunc(1000, update, 0);
	glewExperimental = GL_TRUE;
	glewInit();
	costruisci_scena();
	INIT_VAO();

	glutMainLoop();
}


