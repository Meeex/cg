#pragma once
#include "Lib.h"
#include "Strutture.h"
double  random_range(double min, double max);
void crea_cono(vector<Point>* vertices, vector <GLuint> *indici, vector <ColorRGBA> *Colori, ColorRGBA colore);
void crea_cilindro(vector<Point>* vertices, vector <GLuint> *indici, vector <ColorRGBA>* Colori, ColorRGBA colore);
void crea_toro(vector<Point> *vertices, vector <GLuint> *indici, vector <ColorRGBA> *Colori, ColorRGBA colore);
void crea_sfera(Point centro, Point raggio, vector<Point> *vertici_Sfera, vector <GLuint> *indici_Sfera, vector <ColorRGBA> *Colori_Sfera, ColorRGBA colore);
void crea_cubo(Point *vertices, ColorRGBA *vertex_colors, Indici  *index_vertices);
void crea_piramide(Point *vertices, ColorRGBA *vertex_colors, Indici  *index_vertices);
void crea_piano(Point *vertices, ColorRGBA *vertex_colors, Indici  *index_vertices);
