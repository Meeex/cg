#pragma once
#include"stdafx.h"
#include "Lib.h"
GLuint crea_VAO_Vector(GLuint VAO, vector <Point> vertici, vector <GLuint> indici, vector <ColorRGBA> Colori);
GLuint crea_VAO_Puntatori(GLuint VAO, Point* Vertici, int nv, ColorRGBA* Colori_vertici, Indici* Indici_vertici, int ni);
